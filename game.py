from random import randint

user_name = input("Hi! What is your name?")

for i in range(1,6):
    birth_month = randint(1,12)
    birth_year = randint (1924,2004)
    guess_number = i
    print("Guess " + str(guess_number) + ": " + user_name + ", were you born in " + str(birth_month) + "/" + str(birth_year) + "?  ")
    correct = input("Yes or no?")
    if correct == "yes" or correct == "Yes":
        print("I knew it!")
        exit()
    elif guess_number > 4:
        print("I have other things to do. Good bye.")
        exit()
    else:
        print("Drat! Lemme try again!")
print("I have other things to do. Good bye.")
